function generateCards() {
    const $productCard = $('.product-card');
    const $loadButton = $('#load-button-card');
    let $window = $(window);
    let countProducts ;

    function changeWidth(){
        if ($window.width() <= 767) {
            countProducts = 3;
        } else if ($window.width() < 980) {
            countProducts = 4;
        } else {
            countProducts = 9 ;
        }
        showProducts($productCard, countProducts);
    }

    changeWidth()
    $(window).resize(function () {
        changeWidth()
    });

    $loadButton.on('click', function () {
        showProducts($('.product-card:hidden'), countProducts);
        if($('.product-card:hidden').length === 0){
            $loadButton.hide()
        }

    });

    function showProducts(items, count) {
        for (let i = 0; i < count; i++) {
            $(items[i]).css({display:'block'});
            if (!items[i]) {
                return
            }
        }
    }
}

generateCards();




