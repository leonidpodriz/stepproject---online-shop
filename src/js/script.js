function initTooltips() {
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
}

// Gallery
function createProductGallery(tabs, showBox, likeButton = '#like') {
    createGalleryCarousel(tabs, showBox);
    loadLikes(likeButton, tabs);
    let $elements = $(tabs).children();
    initFirstElement($elements, showBox);
    addLikeEvent(showBox, likeButton)
}
function createGalleryCarousel(tabs, showBox) {
    createArrows(tabs);
    $(tabs).find('div').click( (event) => {
        if (event.target.localName === 'img') {
            doActive(event);
            changeBoxElement($(event.target), $(showBox));
            event.stopPropagation()
        }
    });
}
function createArrows (block) {
    $(block).slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        variableWidth: true,
        prevArrow: '<button class="fas fa-chevron-left inner-gallery__tabs__button"></button>',
        nextArrow: '<button class="fas fa-chevron-right inner-gallery__tabs__button"></button>'
    });
}
function doActive(event) {
    $(event.target).addClass('active').siblings().removeClass('active');
}
function changeBoxElement(element, showBox) {
    let boxImage = showBox.find(".itemImage");
    let boxTitle = showBox.find("h4");
    let boxRate = showBox.find('#rate');
    let boxPrice = showBox.find('#price');
    let boxLastPrice = showBox.find('#lastPrice');
    let boxLikeButton = showBox.find('#like');
    let watchButton = showBox.find('button.view');
    changeBoxID(showBox, element);
    changeImage(boxImage, element);
    changeTitle(boxTitle, element);
    changeRate(boxRate, element);
    changePrice(boxPrice, boxLastPrice, element);
    changeLikeButton(boxLikeButton, element);
    addAllToButtonData(watchButton, boxTitle.text(), boxLastPrice.text(), boxPrice.text(), boxImage.attr('src'));
}
function changeBoxID(showBox, element) {
    showBox.attr('data-id', element.attr('data-id'));
}
function changeImage(image, element) {
    image.attr('src', element.attr('src'));
}
function changeTitle(title, element) {
    title.text(element.attr('data-title'));
}
function changeRate(boxRate, element) {
    let goodRate = '<i class="fas fa-star"></i>';
    let badRate = '<i class="far fa-star"></i>';
    let currentRate = element.attr('data-rate');
    boxRate.children()
        .remove().prevObject
        .append(
            goodRate.repeat(+currentRate) +
            badRate.repeat(5 - currentRate)
        );
}
function changePrice(boxPrice, boxLastPrice, element) {
    boxPrice.text(element.attr('data-price'));
    boxLastPrice.text(element.attr('data-lastPrice'));
}
function changeLikeButton(boxLikeButton, element) {
    boxLikeButton.attr('data-id', element.attr('data-id'));
    if (isLiked(element.attr('data-id')) && boxLikeButton.hasClass('far')) {
        boxLikeButton.toggleClass('far fas active')
    } else if (!isLiked(element.attr('data-id')) && boxLikeButton.hasClass('fas')) {
        boxLikeButton.toggleClass('far fas active')
    }
}
function loadLikes() {
    let likes = localStorage.likes;
    if (likes) {
        return likes.split(' ');
    } else {
        return '';
    }
}
function isLiked(id) {
    return loadLikes().includes(id);
}
function initFirstElement(elements, showBox) {
    let $firstElement = elements.find('.slick-current');  // Work with slick
    $firstElement.toggleClass('active');
    changeBoxElement($firstElement, $(showBox));
}
function addLikeEvent(showBox, likeButton) {
    $(showBox).find(likeButton).click( (event) => {
        let $likeButton = $(event.currentTarget);
      if ($likeButton.hasClass('active')) {
          localStorage.likes = loadLikes().filter(el => el !== $likeButton.attr('data-id')).join(' ');
      } else {
          localStorage.likes += ' ' + $likeButton.attr('data-id');
      }
      $likeButton.toggleClass('active far fas').blur();
      return false;
  });
}
function addAllToButtonData(button, title, lastPrice, price, image) {
    button.attr({
        'data-title': title,
        'data-sale': lastPrice,
        'data-product-cost': price,
        'data-src': image,
    })
}
//<button type="button"  data-title="Aenean Ru Bristique" data-sale="" data-src="./../img/furniture/10.png"  data-product-cost="$15.00"  data-toggle="modal" data-target="#show-modal" class="button-view ml-2 px-2 py-1">Quick View</button>
initTooltips();
// $('#baskedModal').modal();
createProductGallery('#inner-gallery__tabs', '#inner-gallery__content');

